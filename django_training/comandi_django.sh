#!/bin/bash
case $1 in
    start)
        python3 manage.py runserver
        ;;
    web_test)
        firefox http://127.0.0.1:8000/
        ;;
    web_admin)
        firefox http://127.0.0.1:8000/admin/
        ;;
esac