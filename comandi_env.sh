#!/bin/bash
case $1 in
    start)
        source Enviroment/django_training/bin/activate
        ;;
    stop)
        deactivate
        ;;
    env)
        if [ -z "$VIRTUAL_ENV" ]; then
            echo "No virtual environment is active."
            exit 1
        fi
        echo "Virtual environment is active: $VIRTUAL_ENV"
        ;;
    *)
        echo "Usage: $0 {start|stop|env}"
        exit 1
        ;;
esac
